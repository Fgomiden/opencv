#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    //Carrega a imagem
    CommandLineParser parser( argc, argv, "{@input | landscape.jpg | input image}" );
    Mat src = imread( samples::findFile( parser.get<String>( "@input" ) ), IMREAD_COLOR );
    
    //Verifica se tem imagem carregada, se nao tiver o programa fecha
    if( src.empty() )
    {
        return EXIT_FAILURE;
    }
    
    /*Separa a imagem nos tres planos:R,G e B
    Para isso ele separa utilizando a funcao cv::split, que separa um array multi-canal em arrays de canal-unico
     O input sera a imagem dividida em tres canais e o output sera um vetor do Mat*/
    vector<Mat> bgr_planes;
    split( src, bgr_planes );
    
    //Como esta sendo usado o RGB os valores serao no intervalo de 0 a 255. Por isso o tamanho é 256
    int histSize = 256;    
    float range[] = { 0, 256 };//valores entre 0 e 255 
    const float* histRange = { range };
    
    //uniform=true porque o conjunto tem que ter o mesmo tamanho
    //accumulate=false para que o histograma seja limpado no começo
    bool uniform = true, accumulate = false;
    
    //A funcao cv::calcHist calcula um histograma de um conjunto de arrays 
    Mat b_hist, g_hist, r_hist;
    calcHist( &bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate );
    
    //Cria uma imagem para mostrar os histogramas
    int hist_w = 512, hist_h = 400;
    int bin_w = cvRound( (double) hist_w/histSize );
    Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );
    
    //A funcao cv::normalize aplica a normalizacao ou modulo de vetor para que os valores fiquem no intervalo desejado 
    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    
    //Loop que percorre por todo o histograma
    for( int i = 1; i < histSize; i++ )
    {
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ),
              Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ),
              Scalar( 255, 0, 0), 2, 8, 0  );
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(g_hist.at<float>(i-1)) ),
              Point( bin_w*(i), hist_h - cvRound(g_hist.at<float>(i)) ),
              Scalar( 0, 255, 0), 2, 8, 0  );
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(r_hist.at<float>(i-1)) ),
              Point( bin_w*(i), hist_h - cvRound(r_hist.at<float>(i)) ),
              Scalar( 0, 0, 255), 2, 8, 0  );
    }
    //Exibe a imagem
    imshow("Source image", src );
    //exibe o histograma
    imshow("calcHist Demo", histImage );
    waitKey();
    return EXIT_SUCCESS;
}
