#include <iostream>
#include <fstream>
#include <string>

#include <Eigen/Dense>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#ifdef __OPENCV_VERSION_3_0
#	define TOHSV		CV_BGR2HSV
#else
#	define TOHSV		COLOR_BGR2HSV
#endif

using namespace std;
using namespace Eigen;
using namespace cv;

int main(void);

int main(void){

Matrix3d		T1, T2, S;//T1 e T2 = translacao S = escala
Matrix3d		R; //rotacao
Vector3d		p1, p2, p3, p4; //quadrado
Vector3d		pc;
Vector3d		d1, d2, d3, d4;

Mat img = imread("landscape.jpg");
Mat imgFinal = Mat::zeros(img.rows, img.cols, CV_8UC3);
    
double angulo;
double escala;

if (img.empty())
    {
        cerr << "Erro ao carregar img";
    };

int comp = 50;

	cout << "Digite os pontos da base do quadrado.\n";
   	cout << "x: ";
    	cin >> p1[0];
   	cout << "y: ";
    	cin >> p1[1];
    	p1[2] = 1;
    	
    	 cout << "Comprimento do quadrado:\n";
    cin >> comp;

    cout << "Escala para alongar o quadrado:\n";
    cin >> escala;

    cout << "Angulo de rotacao do quadrado:\n";
    cin >> angulo;

    p2 << p1[0], p1[1] + comp, 1;
    p3 << p1[0] + comp, p1[1], 1;
    p4 << p1[0] + comp, p1[1] + comp, 1;

	angulo = angulo * 3.1415 / 180;
    	
	R << cos(angulo), sin(angulo), 0, -sin(angulo), cos(angulo), 0, 0, 0, 1;
	           
	 T1 << 1, 0, 0,
        0, 1, 0,
        -p1[0], -p1[1], 1;

    T2 << 1, 0, 0,
        0, 1, 0,
        p1[0], p1[1], 1;

    S << escala, 0, 0,
        0, escala, 0,
        0, 0, 1;

	
	for (int y = p1[1]; y < p4[1]; y++)
	{
	 for (int x = p1[0]; x < p4[0]; x++)
	 {
	  imgFinal.at<Vec3b>(y, x) = img.at<Vec3b>(y, x);
	  }
	}


	d1 = p1.transpose() * T1 * R * S;
    	d2 = p2.transpose() * T1 * R * S;
    	d3 = p3.transpose() * T1 * R * S;
    	d4 = p4.transpose() * T1 * R * S;

	rectangle(img, Point(p1[0], p1[1]), Point(p4[0], p4[1]), Scalar(255, 255, 255), 3);
    	

	line(imgFinal, Point(d1[0], d1[1]), Point(d2[0], d2[1]), Scalar(0, 0, 255), 3);
	line(imgFinal, Point(d1[0], d1[1]), Point(d3[0], d3[1]), Scalar(0, 255, 0), 3);
	line(imgFinal, Point(d2[0], d2[1]), Point(d4[0], d4[1]), Scalar(255, 0, 0), 3);
	line(imgFinal, Point(d3[0], d3[1]), Point(d4[0], d4[1]), Scalar(0, 0, 255), 3);



	namedWindow( "canvas", WINDOW_NORMAL); 
	// Show our image ins , image );                
	imshow( "canvas", imgFinal );     
	
	waitKey(0);

	return 0;
}

